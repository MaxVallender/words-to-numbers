W o r d s - to - N u m b e r s
==============================


## P r o b l e m


## S o l u t i o n


### Background

	Elf		Zehn	Neun	Acht	Sieben	Sechs	Fünf	Vier	Drei	Zwei	Eins

	n		o	.	r		s		t	.	u		v		w	.	x		y		z

			Mia	|	HunMil			Mil	|	HunTau			Tau	|	Hun

				MilaN	HunMil			MilS	HTF				TauD	HunZ

				|						|						|

	(Zehner					Zehner					Zehner	Zehner		Zehner				Und)


	Hundert Tausend:			HundertTausend    	-> 100 1.000   = 101.000		wrong

	 	 						Hundert * Tausend	-> 100 * 1.000 = 100.000		right


	Hunder Zwanzig Tausend:		Hundert * 20 Tausend								wrong

								Hundert * Tausend + 20 * Tausend					right


### in Code :

	// CopyCode: 00X
	// ActLike:  LeftSide on NumberTokenX

### Problems

 * "null" on the left
 * bis sechsstellig, vielleicht noch Einzelfälle
 * über sechsstellig, ab Million, handelt noch von sich aus, statt linker Seite -> ungültige Zahlen möglich
 * momentan aggiert lediglich Million (und auch TextToken) als linke Seite
 * "minus null" kann nicht verarbeitet werden
 * Nachkommastellen bei ausgeschriebenen Zahlen

## U s e

### Download & Compile & Run

	$ git clone ...Words-to-Numbers

	($ dotnet build)

	$ dotnet run

### Code example

	Words_to_Numbers wtn = new Words_to_Numbers();

	string textWithNumbers = "Zahlen wie minus null vier millionen achtzehnhundert zweiUNDdreißig Euro oder auch - 0 4 00 18 32,123 4 € gehen, geradeso.";

	int debugLevel = 0; // debugLevel is optional: 0, 1, 2 - standard: 0

	List<Token> liste = wtn.run(textWithNumbers, 4, 0);

	foreach (Token t in liste)
		Console.WriteLine(t.ToString(debugLevel));
			
	foreach (Token t in wtn.concatenator(wtn.filter(liste, 2, debugLevel), debugLevel))
		Console.WriteLine(t.ToString(debugLevel));

Output:

	Zahlen wie
	-0
	4001832 euro
	 oder auch
	-4001832,1234 ?
	 gehen
	,

	geradeso
	.
	Zahlen wie
	-0
	4001832 euro
	 oder auch
	-4001832,1234 ?
	 gehen, geradeso.

Full example:

	+using System;
	+using System.Collections.Generic;

		...

		Words_to_Numbers wtn = new Words_to_Numbers();

		int debugLevel = 1; // debugLevel is optional: 0, 1, 2 - standard: 0
		int numberOfRuns = 6; // numberOfRuns is optional: 0 to 10 - standard: 1 (values over 4 prevert)
		int filterLevel = 2; // filterLevel is optimal: 0 to ... - standard: 2

		string textWithNumbers = "Zahlen wie minus null vier millionen achtzehnhundert zweiUNDdreißig Euro oder auch - 0 4 00 18 32,123 4 € gehen, geradeso.";

		List<Token> liste = wtn.run(textWithNumbers, numberOfRuns, debugLevel);

		foreach (Token t in liste)
			Console.WriteLine(t.ToString(debugLevel));

		liste = wtn.filter(liste, filterLevel, debugLevel);
		liste = wtn.concatenator(liste, debugLevel);

		foreach (Token t in liste)
			Console.WriteLine(t.ToString(debugLevel));

		Console.Write("  Input: " + textWithNumbers + "\n Output: ");
		foreach (Token t in liste)
			Console.Write(t.ToString(0));


