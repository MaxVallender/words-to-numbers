using System;
using System.Collections.Generic;
using System.Linq;

namespace Words_to_Numbers {

	public class Dictionaries {
		public static Dictionary<string, Token> Strings = new Dictionary<string, Token>() {
			["einmal"] = TextToken.constructStatic("einmal"),
			["einiges"] = TextToken.constructStatic("einiges"),

			["null"] = NumberTokenEins.constructStatic("null", 0),
			["eins"] = NumberTokenEins.constructStatic("eins", 1),
				["ein"] = NumberTokenEins.constructStatic("ein", 1),
				["eine"] = NumberTokenEins.constructStatic("eine", 1),
				["einen"] = NumberTokenEins.constructStatic("einen", 1),
			["zwei"] = NumberTokenEins.constructStatic("zwei", 2),
				["zwo"] = NumberTokenEins.constructStatic("zwo", 2),
//				["II"] = NumberTokenEins.constructStatic("II", 2),
			["drei"] = NumberTokenEins.constructStatic("drei", 3),
			["vier"] = NumberTokenEins.constructStatic("vier", 4),
			["fünf"] = NumberTokenEins.constructStatic("fünf", 5),
				["fuenf"] = NumberTokenEins.constructStatic("fuenf", 5),
			["sechs"] = NumberTokenEins.constructStatic("sechs", 6),
				["halbes dutzend"] = NumberTokenZwei.constructStatic("halbes dutzend", 6),
			["sieben"] = NumberTokenEins.constructStatic("sieben", 7),
			["acht"] = NumberTokenEins.constructStatic("acht", 8),
			["neun"] = NumberTokenEins.constructStatic("neun", 9),

			// ["zehn"] = NumberTokenZwei.constructStatic("zehn", 10), see below, is saver there
			["elf"] = NumberTokenZwei.constructStatic("elf", 11),
			["zwölf"] = NumberTokenZwei.constructStatic("zwölf", 12),
				["zwoelf"] = NumberTokenZwei.constructStatic("zwoelf", 12),
				["dutzend"] = NumberTokenZwei.constructStatic("dutzend", 12),
			["dreizehn"] = NumberTokenZwei.constructStatic("dreizehn", 13),
			["vierzehn"] = NumberTokenZwei.constructStatic("vierzehn", 14),
			["fünfzehn"] = NumberTokenZwei.constructStatic("fünfzehn", 15),
			["sechzehn"] = NumberTokenZwei.constructStatic("sechzehn", 16),
			["siebzehn"] = NumberTokenZwei.constructStatic("siebzehn", 17),
			["achtzehn"] = NumberTokenZwei.constructStatic("achtzehn", 18),
			["neunzehn"] = NumberTokenZwei.constructStatic("neunzehn", 19),

			["zehn"] = NumberTokenZehner.constructStatic("zehn", 1), // 10
			["zwanzig"] = NumberTokenZehner.constructStatic("zwanzig", 2), // 20
			["dreißig"] = NumberTokenZehner.constructStatic("dreißig", 3), // 30
				["dreissig"] = NumberTokenZehner.constructStatic("dreissig", 3), // 30
			["vierzig"] = NumberTokenZehner.constructStatic("vierzig", 4), // 40
			["fünfzig"] = NumberTokenZehner.constructStatic("fünfzig", 5), // 50
				["fuenfzig"] = NumberTokenZehner.constructStatic("fuenfzig", 5), // 50
			["sechzig"] = NumberTokenZehner.constructStatic("sechzig", 6), // 60
			["siebzig"] = NumberTokenZehner.constructStatic("siebzig", 7), // 70
			["achtzig"] = NumberTokenZehner.constructStatic("achtzig", 8), // 80
			["neunzig"] = NumberTokenZehner.constructStatic("neunzig", 9), // 90

			// Note: spezialfälle (wie 200, viertelmillionen ?) sind bisher noch nicht berücksichtigt, nur faktor 1
			["hundert"] = NumberTokenHundert.constructStatic("hundert", 1), // 100
			["zhundert"] = NumberTokenHundert.constructStatic("zhundert", 2), // 100
				["hunderte"] = NumberTokenHundert.constructStatic("hunderte", 1), // 100
			["tausend"] = NumberTokenTausend.constructStatic("tausend", 1), // 1000
			["million"] = NumberTokenMillion.constructStatic("million", 1), // 1000000
				["millionen"] = NumberTokenMillion.constructStatic("millionen", 1), // 1000000
			// ["milliarde"] = NumberTokenMilliarde.constructStatic("milliarde", 1), // 1000000000 // ToDo
			// 	["milliarden"] = NumberTokenMilliarde.constructStatic("milliarden", 1), // 1000000000
				
			["minus"] = MinusToken.constructStatic("minus"),
			["./."] = MinusToken.constructStatic("./."),

			["komma"] = SeperatorToken.constructStatic("komma"),

			["."] = WhiteSpaceToken.constructStatic("."), // here because as character it would cause problems with .-beginning entries
			["punkt"] = WhiteSpaceToken.constructStatic("punkt"),

			["und"] = UndToken.constructStatic("und"),

			["euro"] = CurrencyToken.constructStatic("euro"),
				["€"] = CurrencyToken.constructStatic("€"), // here, because as character it would cause problems with €-beginning entries
				["€uro"] = CurrencyToken.constructStatic("€uro"), // ist schon als -> KorrekturToken für "uro", welches € verwerten kann?
				["cent"] = CurrencyToken.constructStatic("cent"),
				["eur"] = CurrencyToken.constructStatic("eur"),
				["euro, cent"] = CurrencyToken.constructStatic("euro, cent"),
				["in euro"] = CurrencyToken.constructStatic("in euro"),
				["(in euro)"] = CurrencyToken.constructStatic("(in euro)"),
			["dollar"] = CurrencyToken.constructStatic("dollar"),
			["zentimeter"] = CurrencyToken.constructStatic("zentimeter"),
				["cm"] = CurrencyToken.constructStatic("cm"),
				["dezimeter"] = CurrencyToken.constructStatic("dezimeter"),
				["dm"] = CurrencyToken.constructStatic("dm"), // Note: DMark ?
				["meter"] = CurrencyToken.constructStatic("meter"),
				["m"] = CurrencyToken.constructStatic("m"), // here because as character it would cause problems with m-beginning entries
				["kilometer"] = CurrencyToken.constructStatic("kilometer"),
				["km"] = CurrencyToken.constructStatic("km"),
			["quadratmeter"] = CurrencyToken.constructStatic("quadratmeter"),
				["qm"] = CurrencyToken.constructStatic("qm"),
				["m^2"] = CurrencyToken.constructStatic("m^2"),
				["quadratmeterkilometer"] = CurrencyToken.constructStatic("quadratmeterkilometer"),
			["prozent"] = CurrencyToken.constructStatic("prozent"),

			["€/qm"] = CurrencyToken.constructStatic("€/qm"),

			["viertel"] = ((NumberTokenZwei.constructStatic("viertel", 25, 1, OwnDataObject.constructStatic(null, null, CurrencyToken.constructStatic("%")), default))),
			["hälfte"] = ((NumberTokenZwei.constructStatic("hälfte", 50, 1, OwnDataObject.constructStatic(null, null, CurrencyToken.constructStatic("%")), default))),
				["haelfte"] = ((NumberTokenZwei.constructStatic("haelfte", 50, 1, OwnDataObject.constructStatic(null, null, CurrencyToken.constructStatic("%")), default))),
			["halbe-halbe"] = ((NumberTokenZwei.constructStatic("halbe-halbe", 50, 1, OwnDataObject.constructStatic(null, null, CurrencyToken.constructStatic("%")), default))),
			["fünfzig-fünfzig"] = ((NumberTokenZwei.constructStatic("fünfzig-fünfzig", 50, 1, OwnDataObject.constructStatic(null, null, CurrencyToken.constructStatic("%")), default))), // ToDo: Problem
				["fuenfzig-fuenfzig"] = ((NumberTokenZwei.constructStatic("fuenfzig-fuenfzig", 50, 1, OwnDataObject.constructStatic(null, null, CurrencyToken.constructStatic("%")), default))), // ToDo: Problem
			//["0815"] = new TextToken("0815"), // ToDo: Achtung Falle, Zahlen werden zuvor durch Characters alle einzeln erkannt
		};

		public static Dictionary<string, Token> Characters = new Dictionary<string, Token>() {
			["-"] = MinusToken.constructStatic("-"), // Kurzstrich
			["−"] = MinusToken.constructStatic("−"), // Minusstrich
			["⁒"] = MinusToken.constructStatic("⁒"), // Kaufmännisches Minuszeichen
			["∖"] = MinusToken.constructStatic("∖"), // Differenzmengenzeichen
			// s ?

			[","] = SeperatorToken.constructStatic(","),

			[" "] = WhiteSpaceToken.constructStatic(" "),
			// "." ins Strings
			["_"] = WhiteSpaceToken.constructStatic("_"),

			// "€" in Strings
			["$"] = CurrencyToken.constructStatic("$"),
			["%"] = CurrencyToken.constructStatic("%"),
			//["§"] = CurrencyToken.constructStatic("§"),
			// "m" in Strings

			["0"] = NumberTokenSimple.constructStatic("0", 0),
			["1"] = NumberTokenSimple.constructStatic("1", 1),
			["2"] = NumberTokenSimple.constructStatic("2", 2),
			["3"] = NumberTokenSimple.constructStatic("3", 3),
			["4"] = NumberTokenSimple.constructStatic("4", 4),
			["5"] = NumberTokenSimple.constructStatic("5", 5),
			["6"] = NumberTokenSimple.constructStatic("6", 6),
			["7"] = NumberTokenSimple.constructStatic("7", 7),
			["8"] = NumberTokenSimple.constructStatic("8", 8),
			["9"] = NumberTokenSimple.constructStatic("9", 9),
		};
	}

	public class OwnDataObject {
		public MinusToken minus { get; }
		public NumberTokenDecimals decimals { get; }
		public CurrencyToken currency { get; }

		private OwnDataObject(MinusToken minus, NumberTokenDecimals decimals, CurrencyToken currency) {
			this.minus = minus;
			this.decimals = decimals;
			this.currency = currency;
		}

		public static OwnDataObject constructStatic(MinusToken minus, NumberTokenDecimals decimals, CurrencyToken currency) {
			return new OwnDataObject(minus, decimals, currency);
		}

		public static OwnDataObject constructStatic(OwnDataObject odo) {
			if (odo is OwnDataObject)
				return new OwnDataObject(odo.minus, odo.decimals, odo.currency);
			else
				return null;
		}

		public static OwnDataObject constructStatic() {
			return OwnDataObject.constructStatic(default, default, default);
		}

        public override string ToString() {
            return "Minus: " + (minus is MinusToken) + " | Decimals: [" + (decimals is NumberTokenDecimals ?  decimals.ToString(1) : " ") + "] | Currency: " + (currency is CurrencyToken);
        }
    
	}

	public class NumberTokenSimple : NumberToken {

		public NumberTokenSimple(string s, int i, int hops = default, OwnDataObject odo = default, int? j = default) : base(s, i, hops, odo, j) { ; }

		public new static NumberTokenSimple constructStatic(string s, int i, int hops = 1, OwnDataObject odo = default, int? j = default) {
			if (isValid(i) && isValidJ(j))
				return new NumberTokenSimple(s, i, hops, odo, j);
			else
				return null;
		}

		public override NumberToken construct(string s, int i, int hops = 1, OwnDataObject odo = default, int? j = default) {
			return constructStatic(s, i, hops, odo, j);
		}

		public new static bool isValid(int i) => (0 <= i && i <= int.MaxValue);

		public List<Token> concat(NumberTokenSimple s, Token t) {
			List<Token> ret = new List<Token>();

			if (this.i > 0 && (this.odo == null || this.odo.decimals == null)) {
				OwnDataObject newOdo = this.concatHelperMinusDecimalsCurrency(s.odo);
				NumberToken newToken;
				if (s.i > 0)
					newToken = this.construct(this.s + s.s, this.i * (int)Math.Pow(10, calcDigits(s.i)) + s.i, this.hops + s.hops, newOdo, default);
				else
					newToken = NumberTokenSimple.constructStatic(this.s + s.s, this.i * 10, this.hops + s.hops, newOdo, default);

				if (newOdo is OwnDataObject && newToken is NumberToken) {
					ret.Add(newToken);
					ret.Add(t);
				}
			}

			return ret;
		}
	}

	public abstract class Token {
		public string s { get; }
		public int hops { get; }

		public Token(string s, int hops = default) {
			this.s = s;
			this.hops = hops;
		}

		// default null-concat
		// ToDo: Delete ?
		public virtual Token concat(Token t) {
			//Console.WriteLine("Token.concat(): This is FAKE! The real method must be used");
			return null;
		}

		public virtual List<Token> concat(Token t, Token tt) {
			//Console.WriteLine("Token.concat(): This is FAKE! The real method must be used");
			return new List<Token>();
		}

		public virtual string ToString(int debug = 0, string special = default) {
			if (debug == 0)
				return s;
			else if (debug == 1)
				return  "[" + ToString(0) + "]";
			else //if (debug == 2)
				return ToString(1) + " - [" + this.GetType().ToString().Substring(17, this.GetType().ToString().Length - 17) + " | s: \"" + s + "\" | spezial: " + special + "]";
		}
	}

	public class TextToken : Token {
		public TextToken(string s, int hops = 0) : base(s, hops) { ; }

		public static TextToken constructStatic(string s, int hops = default) {
			return new TextToken(s);
		}

		public TextToken construct(string s) {
			return constructStatic(s);
		}

		public TextToken concat(TextToken t) => new TextToken(s + t.s);

// ToDo: both do the same
		public List<Token> concat(UndToken u, TextToken t) {
			List<Token> ret = new List<Token>();
		 	ret.Add( new TextToken(this.s + u.s + t.s) );
			return ret;
		}

		public List<Token> concat(WhiteSpaceToken ws, TextToken t) {
			List<Token> ret = new List<Token>();
			ret.Add( new TextToken(this.s + ws.s + t.s) );
			return ret;
		}

		public List<Token> concat(WhiteSpaceToken ws, NumberToken n) {
			List<Token> ret = new List<Token>();
			ret.Add(new TextToken(this.s + ws.s));
			ret.Add(n);
			return ret;
		}

		public List<Token> concat(WhiteSpaceToken ws, CurrencyToken c) {
			List<Token> ret = new List<Token>();
			ret.Add(new TextToken(this.s + ws.s));
			ret.Add(c);
			return ret;
		}

		// Note: ActLike: LeftSide on NumberTokenSimple - special focus on zeros
		public List<Token> concat(NumberTokenSimple s, NumberTokenSimple ss) {
			List<Token> ret = new List<Token>();

			if (s is NumberTokenSimple && s.i == 0 && ss is NumberTokenSimple) {
				OwnDataObject newOdo = s.concatHelperMinusDecimalsCurrency(ss.odo);
				NumberToken newToken = ss.construct(s.s + ss.s, ss.i, s.hops + ss.hops, newOdo, ss.j);
				if (newOdo is OwnDataObject && newToken is NumberTokenSimple) {
					ret.Add(this);
					ret.Add(newToken);
				}
			} else
			if (s is NumberTokenSimple && s.i > 0 && (s.odo == null || s.odo.decimals == null) && ss is NumberTokenSimple && (s.odo == null || s.odo.decimals == null)) {
				OwnDataObject newOdo = s.concatHelperMinusDecimalsCurrency(ss.odo);
				NumberToken newToken;
				if (ss.i > 0)
					newToken = NumberTokenSimple.constructStatic(s.s + ss.s, s.i * (int)Math.Pow(10, s.calcDigits(ss.i)) + ss.i, this.hops + ss.hops, newOdo, default);
				else
					newToken = NumberTokenSimple.constructStatic(s.s + ss.s, s.i *10, this.hops + ss.hops, newOdo, default);

				if (newOdo is OwnDataObject && newToken is NumberToken) {
					ret.Add(this);
					ret.Add(newToken);
				}
			}

			return ret;
		}

		/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
		 *			CopyCode only !!
		 */


		// Note: ActLike: LeftSide - CopyCode: 003 1
		public virtual List<Token> concat(NumberTokenEins e, NumberTokenHundert t) => this.concat( (NumberTokenDrei)e , t);
		// Note: ActLike: LeftSide - CopyCode: 003 2
		public virtual List<Token> concat(NumberTokenZehner z, NumberTokenHundert t) => this.concat( (NumberTokenDrei)z, t);
		// Note: ActLike: LeftSide - CopyCode: 003 3
		public virtual List<Token> concat(NumberTokenZwei z, NumberTokenHundert t) => this.concat( (NumberTokenDrei)z, t);
		// Note: ActLike: LeftSide - CopyCode: 003 4
		public virtual List<Token> concat(NumberTokenHundert h, NumberTokenHundert t) => this.concat( (NumberTokenDrei)h, t);
		// Note: ActLike: LeftSide - CopyCode: 003 5
		public virtual List<Token> concat(NumberTokenHundertZwei hz, NumberTokenHundert t) => this.concat( (NumberTokenDrei)hz, t);

		// Note: ActLike: LeftSide - ActLike: NumberTokenFünf - CopyCode: 003 6
		public virtual List<Token> concat(NumberTokenDrei d, NumberTokenHundert t) {
			List<Token> ret = new List<Token>();

			if (d is NumberTokenDrei) {
				ret.Add(this);
				ret.Add(d);
				ret.Add(t);
			}

			return ret;
		}

		// Note: ActLike: LeftSide - CopyCode: 003 1
		public virtual List<Token> concat(NumberTokenEins e, NumberTokenHundertZwei t) => this.concat( (NumberTokenDrei)e , t);
		// Note: ActLike: LeftSide - CopyCode: 003 2
		public virtual List<Token> concat(NumberTokenZehner z, NumberTokenHundertZwei t) => this.concat( (NumberTokenDrei)z, t);
		// Note: ActLike: LeftSide - CopyCode: 003 3
		public virtual List<Token> concat(NumberTokenZwei z, NumberTokenHundertZwei t) => this.concat( (NumberTokenDrei)z, t);
		// Note: ActLike: LeftSide - CopyCode: 003 4
		public virtual List<Token> concat(NumberTokenHundert h, NumberTokenHundertZwei t) => this.concat( (NumberTokenDrei)h, t);
		// Note: ActLike: LeftSide - CopyCode: 003 5
		public virtual List<Token> concat(NumberTokenHundertZwei hz, NumberTokenHundertZwei t) => this.concat( (NumberTokenDrei)hz, t);

		// Note: ActLike: LeftSide - ActLike: NumberTokenFünf - CopyCode: 003 6
		public virtual List<Token> concat(NumberTokenDrei d, NumberTokenHundertZwei t) {
			List<Token> ret = new List<Token>();

			if (d is NumberTokenDrei) {
				ret.Add(this);
				ret.Add(d);
				ret.Add(t);
			}

			return ret;
		}

		// Note: ActLike: LeftSide - CopyCode: 001 1
		public virtual List<Token> concat(NumberTokenEins e, NumberTokenTausend t) => this.concat( (NumberTokenDrei)e , t);
		// Note: ActLike: LeftSide - CopyCode: 001 2
		public virtual List<Token> concat(NumberTokenZehner z, NumberTokenTausend t) => this.concat( (NumberTokenDrei)z, t);
		// Note: ActLike: LeftSide - CopyCode: 001 3
		public virtual List<Token> concat(NumberTokenZwei z, NumberTokenTausend t) => this.concat( (NumberTokenDrei)z, t);
		// Note: ActLike: LeftSide - CopyCode: 001 4
		public virtual List<Token> concat(NumberTokenHundert h, NumberTokenTausend t) => this.concat( (NumberTokenDrei)h, t);
		// Note: ActLike: LeftSide - CopyCode: 001 5
		public virtual List<Token> concat(NumberTokenHundertZwei hz, NumberTokenTausend t) => this.concat( (NumberTokenDrei)hz, t);

		// Note: ActLike: LeftSide - CopyCode: 001 6
		public virtual List<Token> concat(NumberTokenDrei d, NumberTokenTausend t) {
			List<Token> ret = new List<Token>();

			if (d is NumberTokenDrei) {
				ret.Add(this);
				ret.Add(d);
				ret.Add(t);
			}

			return ret;
		}

		// Note: ActLike: LeftSide - CopyCode: 001 1
		public virtual List<Token> concat(NumberTokenEins e, NumberTokenTausendDrei td) => this.concat( (NumberTokenDrei)e , td);
		// Note: ActLike: LeftSide - CopyCode: 001 2
		public virtual List<Token> concat(NumberTokenZehner z, NumberTokenTausendDrei td) => this.concat( (NumberTokenDrei)z, td);
		// Note: ActLike: LeftSide - CopyCode: 001 3
		public virtual List<Token> concat(NumberTokenZwei z, NumberTokenTausendDrei td) => this.concat( (NumberTokenDrei)z, td);
		// Note: ActLike: LeftSide - CopyCode: 001 4
		public virtual List<Token> concat(NumberTokenHundert h, NumberTokenTausendDrei td) => this.concat( (NumberTokenDrei)h, td);
		// Note: ActLike: LeftSide - CopyCode: 001 5
		public virtual List<Token> concat(NumberTokenHundertZwei hz, NumberTokenTausendDrei td) => this.concat( (NumberTokenDrei)hz, td);

		// Note: ActLike: LeftSide - CopyCode: 001 6
		public virtual List<Token> concat(NumberTokenDrei d, NumberTokenTausendDrei td) {
			List<Token> ret = new List<Token>();

			if (d is NumberTokenDrei) {
				ret.Add(this);
				ret.Add(d);
				ret.Add(td);
			}

			return ret;
		}
	}

	public class MinusToken : Token {
		public string symbol { get; }

		private MinusToken(string s, string symbol, int hops = 1) : base(s, hops) {
			this.symbol = symbol;
		}

		public static MinusToken constructStatic(string s, int hops = default) => constructStatic(s, s);

		public static MinusToken constructStatic(string s, string symbol, int hops = default) {
			return new MinusToken(s, symbol);
		}
		
		public List<Token> concat(NumberToken n, Token t) {
			List<Token> ret = new List<Token>();

			if (n is NumberToken) {
				OwnDataObject newOdo = n.concatHelperMinusDecimalsCurrency(this, null, null);
				NumberToken newToken = n.construct(this.s + n.s, n.i, this.hops + n.hops, newOdo, n.j);
				if (newOdo is OwnDataObject && newToken is NumberToken) {
					ret.Add(newToken);
					ret.Add(t);
				}
			}

			return ret;
		}

		public List<Token> concat(WhiteSpaceToken ws, NumberToken n) {
			List<Token> ret = new List<Token>();

			MinusToken newToken = MinusToken.constructStatic(this.s + ws.s, this.symbol);
			if (newToken is MinusToken) {
				ret.Add(newToken);
				ret.Add(n);
			}

			return ret;
		}

		public override string ToString(int debug = 0, string special = default) {
			if (debug == 0)
				return symbol;
			else if (debug == 1)
				return  "[" + ToString(0) + "]";
			else //if (debug == 2)
				return base.ToString(2, "symbol: " + symbol);
		}
	}

	public class SeperatorToken : Token {
		public string symbol { get; }

		private SeperatorToken(string s, string symbol, int hops = 0) : base(s, hops) {
			this.symbol = symbol;
		}

		public static SeperatorToken constructStatic(string s, int hops = default) => constructStatic(s, s);

		public static SeperatorToken constructStatic(string s, string symbol, int hops = default) {
			return new SeperatorToken(s, symbol);
		}

		public List<Token> concat(NumberToken n, Token t) {
			List<Token> ret = new List<Token>();

			if (n is NumberToken && (n.odo == null || n.odo.decimals == null) && t != null && ! (t is NumberToken)) { // ToDo: only for Decimals - what des this comment mean?
				OwnDataObject newOdo = n.concatHelperMinusDecimalsCurrency(null, null, null);
				NumberTokenDecimals newToken = NumberTokenDecimals.constructStatic(this.s + n.s, n.i, n.hops, newOdo, n.j);
				if (newOdo is OwnDataObject && newToken is NumberTokenDecimals) {
					ret.Add(newToken);
					ret.Add(t);
				}
			}

			return ret;
		}
	}

	public class CurrencyToken : Token {
		public string symbol { get; }

		private CurrencyToken(string s, string symbol, int hops = 1) : base(s, hops) {
			this.symbol = symbol;
		}

		public static CurrencyToken constructStatic(string s, int hops = default) => constructStatic(s, s, hops);

		public static CurrencyToken constructStatic(string s, string symbol, int hops = default) {
			return new CurrencyToken(s, symbol);
		}

		public List<Token> concat(WhiteSpaceToken ws, NumberToken n) {
			List<Token> ret = new List<Token>();
			ret.Add( CurrencyToken.constructStatic(this.s + ws.s, this.symbol) );
			ret.Add(n);
			return ret;
		}

		public List<Token> concat(NumberToken n, Token t) {
			List<Token> ret = new List<Token>();

			if (n is NumberToken && t != null && ! (t is NumberToken)) {
				OwnDataObject newOdo = n.concatHelperMinusDecimalsCurrency(null, null, this);
				NumberToken newToken = n.construct(this.s + n.s, n.i, n.hops + 1, newOdo, n.j);
				if (newOdo is OwnDataObject && newToken is NumberToken) {
					ret.Add(newToken);
					ret.Add(t);
				}
			}
			
			return ret;
		}

		public override string ToString(int debug = 0, string special = default) {
			if (debug == 0)
				return " " + symbol;
			else if (debug == 1)
				return  "[(in" + ToString(0) + ")]";
			else //if (debug == 2)
				return base.ToString(2, "symbol: " + symbol);
		}
	}

	public abstract class NumberToken : Token {
		public int i { get; }
		public OwnDataObject odo { get; }
		public int? j { get; }

		protected NumberToken(string s, int i, int hops = 1, OwnDataObject odo = default, int? j = default) : base(s, hops) {
			this.i = i;
			this.odo = OwnDataObject.constructStatic(odo);
			this.j = j;
		}

		public static bool isValid(int i) => (0 <= i && i <= 9);

		// ToDo: isValid for j everytime to avoid stupid errors
		public static bool isValidJ(int? j) => (j.HasValue == false);

		public static NumberToken constructStatic(string s, int i, int hops = 1, OwnDataObject odo = default, int? j = default) {
			Console.WriteLine("NumberToken.constructStatic(): This is FAKE! The real method must be used");
			return null;
		}

		public abstract NumberToken construct(string s, int i, int hops, OwnDataObject odo, int? j);

		public virtual int calcIJ() => i;

		public int calcDigits(int number) {

			return (int)Math.Floor(Math.Log10((double)number) + 1);
		}

		public virtual OwnDataObject concatHelperMinusDecimalsCurrency(OwnDataObject odo) {
			return this.concatHelperMinusDecimalsCurrency(this.odo, odo);
		}

		public virtual OwnDataObject concatHelperMinusDecimalsCurrency(OwnDataObject o, OwnDataObject oo) {
			if (o is OwnDataObject)
				if (oo is OwnDataObject)
					return concatHelperMinusDecimalsCurrency(o.minus, oo.minus, o.decimals, oo.decimals, o.currency, oo.currency);
				else
					//return concatHelperMinusDecimalsCurrency(o.minus, null, o.decimals, null, o.currency, null);
					return o;
			else
				if (oo is OwnDataObject)
					//return concatHelperMinusDecimalsCurrency(null, oo.minus, null, oo.decimals, null, oo.currency);
					return oo;
				else
					// Note: By the progress of concat(enation) null is a signal for errors. So here is an not-null object needed. (Parameter of object allowed to be null)
					return OwnDataObject.constructStatic(null, null, null);
		}

		public virtual OwnDataObject concatHelperMinusDecimalsCurrency(MinusToken minus, NumberTokenDecimals decimals, CurrencyToken currency) {
			if (this.odo is OwnDataObject)
				return concatHelperMinusDecimalsCurrency(this.odo.minus, minus, this.odo.decimals, decimals, this.odo.currency, currency);
			else
				return OwnDataObject.constructStatic(minus, decimals, currency);
		}

		public virtual OwnDataObject concatHelperMinusDecimalsCurrency(MinusToken m, MinusToken mm, NumberTokenDecimals d, NumberTokenDecimals dd, CurrencyToken c, CurrencyToken cc) {
			bool valid1, valid2, valid3;
			MinusToken newMinus = concatHelperMinus(m, mm, out valid1);
			NumberTokenDecimals newDecimals = concatHelperDecimals(d, dd, out valid2);
            CurrencyToken newCurrency = concatHelperCurrency(c, cc, out valid3);

			if (valid1 && valid2 && valid3)
				return OwnDataObject.constructStatic(newMinus, newDecimals, newCurrency);
			else
				return null;
		}

		public MinusToken concatHelperMinus(MinusToken m, MinusToken mm, out bool valid) {
			if (m != null && mm != null) {
				valid = false;
			} else {
				valid = true;
				if (m is MinusToken && mm == null)
					return m;
				else if (m == null && mm is MinusToken)
					return mm;
			}
			return null;
		}

		public NumberTokenDecimals concatHelperDecimals(NumberTokenDecimals d, NumberTokenDecimals dd, out bool valid) {
			if (d != null && dd != null) {
				valid = false;
			} else {
				valid = true;
				if (d is NumberTokenDecimals && dd == null)
					return d;
				else if (d == null && dd is NumberTokenDecimals)
					return dd;
			}
			return null;
		}

		public CurrencyToken concatHelperCurrency(CurrencyToken c, CurrencyToken cc, out bool valid) {
			if (c != null && cc != null) {
				valid = false;
			} else {
				valid = true;
				if (c is CurrencyToken && cc == null)
					return c;
				else if (c == null && cc is CurrencyToken)
					return cc;
			}
			return null;
		}

		public virtual List<Token> concat(WhiteSpaceToken ws, TextToken t) {
			List<Token> ret = new List<Token>();

			if (ws is WhiteSpaceToken && t is TextToken) {
				TextToken newToken = TextToken.constructStatic(ws.s + t.s);
				if (newToken is TextToken) {
					ret.Add(this);
					ret.Add(newToken);
				}
			}

			return ret;
		}

		public virtual List<Token> concat(WhiteSpaceToken ws, MinusToken m) {
			List<Token> ret = new List<Token>();

			if (ws is WhiteSpaceToken && m is MinusToken) {
				MinusToken newToken = MinusToken.constructStatic(ws.s + m.s, m.symbol);
				if (newToken is MinusToken) {
					ret.Add(this);
					ret.Add(newToken);
				}
			}

			return ret;
		}

		public virtual List<Token> concat(WhiteSpaceToken ws, UndToken u) {
			List<Token> ret = new List<Token>();

			if (ws is WhiteSpaceToken && u is UndToken) {
				UndToken newToken = UndToken.constructStatic(ws.s + u.s, u.symbol);
				if (newToken is UndToken) {
					ret.Add(this);
					ret.Add(newToken);
				}
			}

			return ret;
		}

		public virtual List<Token> concat(WhiteSpaceToken ws, CurrencyToken c) {
			List<Token> ret = new List<Token>();

			if (ws is WhiteSpaceToken && c is CurrencyToken) {
				CurrencyToken newToken = CurrencyToken.constructStatic(ws.s + c.s, c.symbol);
				if (newToken is CurrencyToken) {
					ret.Add(this);
					ret.Add(newToken);
				}
			}
			
			return ret;
		}

		public virtual List<Token> concat(WhiteSpaceToken ws, NumberToken n) {
			List<Token> ret = new List<Token>();

			if (ws is WhiteSpaceToken) {
				NumberToken newToken = this.construct(this.s + ws.s, this.i, this.hops, this.odo, this.j);
				if (newToken is NumberToken) {
					ret.Add(newToken);
					ret.Add(n);
				}
			}
			
			return ret;
		}

		public virtual List<Token> concat(MinusToken m, Token t) {
			List<Token> ret = new List<Token>();

			if (m is MinusToken && t != null && ! (t is NumberToken)) {
				OwnDataObject newOdo = this.concatHelperMinusDecimalsCurrency(m, null, null);
				NumberToken newToken = this.construct(this.s + m.s, this.i, this.hops + 1, newOdo, this.j);
				if (newOdo is OwnDataObject && newToken is NumberToken) {
					ret.Add(newToken);
					ret.Add(t);
				}
			}

			return ret;
		}

		public virtual List<Token> concat(CurrencyToken c, Token t) {
			List<Token> ret = new List<Token>();

			if (c is CurrencyToken && t != null && ! (t is NumberToken)) {
				OwnDataObject newOdo = this.concatHelperMinusDecimalsCurrency(null, null, c);
				NumberToken newToken = this.construct(this.s, this.i, this.hops + 1, newOdo, this.j);
				if (newOdo is OwnDataObject && newToken is NumberToken) { 
					ret.Add(newToken);
					ret.Add(t);
				}
			}

			return ret;
		}

		public List<Token> concat(NumberTokenDecimals d, Token t) {
			List<Token> ret = new List<Token>();

			if (d is NumberTokenDecimals && t != null && ! (t is NumberToken) ) {
				OwnDataObject newOdo = this.concatHelperMinusDecimalsCurrency(d.odo.minus, d, d.odo.currency);
				NumberToken newToken = this.construct(this.s + d.s, this.i, this.hops + d.hops, newOdo, this.j);
				if (newOdo is OwnDataObject && newToken is NumberToken) {
					ret.Add(newToken);
					ret.Add(t);
				}
			}

			return ret;
		}

		public double calcMinusIJDecimals(int exponent = 1) { // ToDo: Hack
			double decimals = default;

			if (odo is OwnDataObject && odo.decimals is NumberTokenDecimals) {
				int divide = 1;

				while ( ((double)(odo.decimals.i)) / divide >= 1 )
					divide *= 10;

				decimals = ((double)(odo.decimals.i) / divide);
			}

			return ( (odo is OwnDataObject && odo.minus is MinusToken) ? (double)-1 : (double)+1 ) * ( (double)(calcIJ()) + decimals );	
		}

		public override string ToString(int debug = 0, string special = default) {
			if (debug == 0)
				return calcMinusIJDecimals().ToString() + ( (odo is OwnDataObject && odo.currency is CurrencyToken) ? odo.currency.ToString() : "" );
			else if (debug == 1)
				return "[" + ToString(0) + "]";
			else //if (debug == 2)
				return ToString(1) + " - [" + this.GetType().ToString().Substring(17, this.GetType().ToString().Length - 17) + " | s: \"" + s + "\" | i: " + i + " | hops: " + hops + " | " + ( odo is OwnDataObject ? odo.ToString() : "no Minus/Decimals/Currency" ) + " | j: " + j + "]";
		}
	}

	public class NumberTokenEins : NumberToken {
		private NumberTokenEins(string s, int i, int hops = 1, OwnDataObject odo = default, int? j = default) : base(s, i, hops, odo, j) { ; }

		public new static NumberTokenEins constructStatic(string s, int i, int hops = 1, OwnDataObject odo = default, int? j = default) {
			if (isValid(i) && isValidJ(j))
				return new NumberTokenEins(s, i, hops, odo, j);
			else
				return null;
		}

		public override NumberToken construct(string s, int i, int hops = 1, OwnDataObject odo = default, int? j = default) {
			return constructStatic(s, i, hops, odo, j);
		}

		public static implicit operator NumberTokenZwei(NumberTokenEins e) => NumberTokenZwei.constructStatic(e.s, e.i, e.hops, e.odo, default);
		public static implicit operator NumberTokenDrei(NumberTokenEins e) => NumberTokenDrei.constructStatic(e.s, e.i, e.hops, e.odo, default);
		public static implicit operator NumberTokenVier(NumberTokenEins e) => NumberTokenVier.constructStatic(e.s, e.i, e.hops, e.odo, default);
		public static implicit operator NumberTokenFünf(NumberTokenEins e) => NumberTokenFünf.constructStatic(e.s, e.i, e.hops, e.odo, default);
		public static implicit operator NumberTokenSechs(NumberTokenEins e) => NumberTokenSechs.constructStatic(e.s, e.i, e.hops, e.odo, default);
		public static implicit operator NumberTokenSieben(NumberTokenEins e) => NumberTokenSieben.constructStatic(e.s, e.i, e.hops, e.odo, default);
		public static implicit operator NumberTokenAcht(NumberTokenEins e) => NumberTokenAcht.constructStatic(e.s, e.i, e.hops, e.odo, default);
		public static implicit operator NumberTokenNeun(NumberTokenEins e) => NumberTokenNeun.constructStatic(e.s, e.i, e.hops, e.odo, default);
		public static implicit operator NumberTokenZehn(NumberTokenEins e) => NumberTokenZehn.constructStatic(e.s, e.i, e.hops, e.odo, default);

		public virtual List<Token> concat(UndToken u, NumberTokenZehner z) {
			List<Token> ret = new List<Token>();

			if (u is UndToken && z is NumberTokenZehner) {
				OwnDataObject newOdo = this.concatHelperMinusDecimalsCurrency(z.odo);
				NumberTokenZwei newToken = NumberTokenZwei.constructStatic(this.s + u.s + z.s, z.i * 10 + this.i, this.hops + z.hops, newOdo, default);
				if (newOdo is OwnDataObject && newToken is NumberTokenZwei) {
					ret.Add(newToken);
				}
			} else
				return null;

			return ret;
		}

		public virtual List<Token> concat(NumberTokenHundert h, Token t) {
			return concat((NumberTokenHundertZwei)h, t);
		}

		// ToDo: Hundert transforrm itself to HundertZwei
		public virtual List<Token> concat(NumberTokenHundertZwei hz, Token t) {
			List<Token> ret = new List<Token>();

			if (hz is NumberTokenHundertZwei && NumberTokenHundertZwei.isValidJ(hz.j) && t != null && ! (t is NumberToken)) {
				OwnDataObject newOdo = this.concatHelperMinusDecimalsCurrency(hz.odo);
				NumberTokenDrei newToken = NumberTokenDrei.constructStatic(this.s + hz.s, this.i * (hz.i * 100) + hz.j.Value, this.hops + hz.hops, newOdo, default);
				if (newOdo is OwnDataObject && newToken is NumberTokenDrei) {
					ret.Add(newToken);
					ret.Add(t);
				}
			}

			return ret;
		}

		public virtual List<Token> concat(NumberTokenTausendDrei td, Token t) { // ToDo: Allowed? -> Vier, problem with Eins before, when Eins is needed for process
			List<Token> ret = new List<Token>();

			if (td is NumberTokenTausendDrei && NumberTokenTausendDrei.isValidJ(td.j) && t != null && ! (t is NumberToken)) {
				OwnDataObject newOdo = this.concatHelperMinusDecimalsCurrency(td.odo);
				NumberTokenVier newToken = NumberTokenVier.constructStatic(this.s + td.s, this.i * (td.i * 1000) + td.j.Value, this.hops + td.hops, newOdo, default);
				if (newOdo is OwnDataObject && newToken is NumberTokenVier) {
					ret.Add(newToken);
					ret.Add(t);
				}
			}

			return ret;
		}

		public virtual List<Token> concat(NumberTokenHundertTausendFünf htf, Token t) {
			List<Token> ret = new List<Token>();

			if (htf is NumberTokenHundertTausendFünf && NumberTokenHundertTausendFünf.isValidJ(htf.j) && t != null && ! (t is NumberToken)) {
				OwnDataObject newOdo = this.concatHelperMinusDecimalsCurrency(htf.odo);
				NumberTokenSechs newToken = NumberTokenSechs.constructStatic(this.s + htf.s, this.i * (htf.i * 100_000) + htf.j.Value, this.hops + htf.hops, newOdo, default);
				if (newOdo is OwnDataObject && newToken is NumberTokenSechs) {
					ret.Add(newToken);
					ret.Add(t);
				}
			}

			return ret;
		}

		public virtual List<Token> concat(NumberTokenMillionSechs ms, Token t) { // ToDo: Is this allowed? Because before Eins can be something else
			List<Token> ret = new List<Token>();

			if (ms is NumberTokenMillionSechs && NumberTokenMillionSechs.isValidJ(ms.j) && t != null && ! (t is NumberToken)) {
				OwnDataObject newOdo = this.concatHelperMinusDecimalsCurrency(ms.odo);
				NumberTokenSieben newToken = NumberTokenSieben.constructStatic(this.s + ms.s, this.i * (ms.i * 1_000_000) + ms.j.Value, this.hops + ms.hops, newOdo, default);
				if (newOdo is OwnDataObject && newToken is NumberTokenSieben) {
					ret.Add(newToken);
					ret.Add(t);
				}
			}

			return ret;
		}
	}

	public class UndToken : Token {
		public string symbol { get; }

		private UndToken(string s, string symbol, int hops = 0) : base(s, hops) { ; }

		public static UndToken constructStatic(string s, int hops = default) => constructStatic(s, s);

		public static UndToken constructStatic(string s, string symbol, int hops = default) {
			return new UndToken(s, symbol);
		}

		public List<Token> concat(WhiteSpaceToken ws, Token t) {
			List<Token> ret = new List<Token>();
			ret.Add(UndToken.constructStatic(this.s + ws.s));
			ret.Add(t);
			return ret;
		}
	}

	public class WhiteSpaceToken : Token {
		private WhiteSpaceToken(string s, int hops = 0) : base(s, hops) { ; }

		public static WhiteSpaceToken constructStatic(string s, int hops = default) {
			return new WhiteSpaceToken(s);
		}
	}

	public class NumberTokenZehner : NumberToken {
		private NumberTokenZehner(string s, int i, int hops = 1, OwnDataObject odo = default, int? j = default) : base(s, i, hops, odo, j) { ; }

		public new static NumberTokenZehner constructStatic(string s, int i, int hops = 1, OwnDataObject odo = default, int? j = default) {
			if (isValid(i) && isValidJ(j))
				return new NumberTokenZehner(s, i, hops, odo, j);
			else
				return null;
		}

		public override NumberToken construct(string s, int i, int hops = 1, OwnDataObject odo = default, int? j = default) {
			return constructStatic(s, i, hops, odo, j);
		}

        public override int calcIJ() => i * 10;

		public static implicit operator NumberTokenZwei(NumberTokenZehner z) => NumberTokenZwei.constructStatic(z.s, z.calcIJ(), z.hops, z.odo, default);
		public static implicit operator NumberTokenDrei(NumberTokenZehner z) => NumberTokenDrei.constructStatic(z.s, z.calcIJ(), z.hops, z.odo, default);
		public static implicit operator NumberTokenTausendDrei(NumberTokenZehner z) => NumberTokenTausendDrei.constructStatic(z.s, 0, z.hops, z.odo, z.calcIJ());

		public virtual List<Token> concat(NumberTokenMillionSechs ms, Token t) {
				return ((NumberTokenZwei)this).concat(ms, t);
		}
	}

	public class NumberTokenHundert : NumberToken {
		private NumberTokenHundert(string s, int i, int hops = 1, OwnDataObject odo = default, int? j = default) : base(s, i, hops, odo, j) { ; }

		public new static NumberTokenHundert constructStatic(string s, int i, int hops = 1, OwnDataObject odo = default, int? j = default) {
			if (isValid(i) && isValidJ(j))
				return new NumberTokenHundert(s, i, hops, odo, j);
			else
				return null;
		}

		public override NumberToken construct(string s, int i, int hops = 1, OwnDataObject odo = default, int? j = default) {
			return constructStatic(s, i, hops, odo, j);
		}

		public override int calcIJ() => i * 100;

		public static implicit operator NumberTokenHundertZwei(NumberTokenHundert h) => NumberTokenHundertZwei.constructStatic(h.s, h.i, h.hops, h.odo, 0);
		public static implicit operator NumberTokenDrei(NumberTokenHundert h) => NumberTokenDrei.constructStatic(h.s, h.calcIJ(), h.hops, h.odo, default);

		public virtual List<Token> concat(NumberTokenZehner z, Token t) {
			return concat( (NumberTokenZwei)z , t);
		}

		// ToDo: both methods are turning hundert and something into drei, number before hundert does not work!!
		// ToDo: mehr als Eins und Zwei ?
		public virtual List<Token> concat(NumberTokenZwei z, Token t) {
			List<Token> ret = new List<Token>();

			if (z is NumberTokenZwei && t != null && ! (t is NumberToken)) {
				OwnDataObject newOdo = this.concatHelperMinusDecimalsCurrency(z.odo);
				NumberTokenHundertZwei newToken = NumberTokenHundertZwei.constructStatic(this.s + z.s, this.i, this.hops + z.hops, newOdo, z.i);
				if (newOdo is OwnDataObject && newToken is NumberTokenHundertZwei) {
					ret.Add(newToken);
					ret.Add(t);
				}
			}

			return ret;
		}

		// Note: ActLike: NumberTokenHundertTausend - CopyCode: 002 1
		public virtual List<Token> concat(NumberTokenTausend t, Token tt) => concat((NumberTokenTausendDrei)t, tt);

		// Note: ActLike: NumberTokenHundertTausend - CopyCode: 002 2
		public virtual List<Token> concat(NumberTokenTausendDrei f, Token t) {
			List<Token> ret = new List<Token>();

			if (f is NumberTokenTausendDrei && t != null && !(t is NumberToken)) {
				OwnDataObject newOdo = this.concatHelperMinusDecimalsCurrency(f.odo);
				NumberTokenHundertTausendFünf newToken = NumberTokenHundertTausendFünf.constructStatic(this.s + f.s, this.i * f.i, this.hops + f.hops, newOdo, f.j.Value);
				if (newOdo is OwnDataObject && newToken is NumberTokenHundertTausendFünf) {
					ret.Add(newToken);
					ret.Add(t);
				}
			}

			return ret;
		}

		public virtual List<Token> concat(NumberTokenVier v, Token t) {
			return concat((NumberTokenFünf)v, t);
		}

		public virtual List<Token> concat(NumberTokenFünf f, Token t) {
			List<Token> ret = new List<Token>();

			if (f is NumberTokenFünf && t != null && !(t is NumberToken)) {
				OwnDataObject newOdo = this.concatHelperMinusDecimalsCurrency(f.odo);
				NumberTokenHundertTausendFünf newToken = NumberTokenHundertTausendFünf.constructStatic(this.s + f.s, this.i, this.hops + f.hops, newOdo, f.i);
				if (newOdo is OwnDataObject && newToken is NumberTokenHundertTausendFünf) {
					ret.Add(newToken);
					ret.Add(t);
				}
			}

			return ret;
		}
	}

	public class NumberTokenHundertZwei : NumberToken {

		private NumberTokenHundertZwei(string s, int i, int hops = 1, OwnDataObject odo = default, int? j = default) : base(s, i, hops, odo, j) { ; }

		public new static bool isValidJ(int? j) => (j.HasValue && 0 <= j.Value && j.Value <= 99);

		public new static NumberTokenHundertZwei constructStatic(string s, int i, int hops = 1, OwnDataObject odo = default, int? j = default) {
			if (isValid(i) && isValidJ(j))
				return new NumberTokenHundertZwei(s, i, hops, odo, j);
			else
				return null;
		}

        public override NumberToken construct(string s, int i, int hops = 1, OwnDataObject odo = default, int? j = default) {
            return constructStatic(s, i, hops, odo, j);
        }

		public override int calcIJ() => i * 100 + j.Value;

		public static implicit operator NumberTokenDrei(NumberTokenHundertZwei hz) => NumberTokenDrei.constructStatic(hz.s, hz.calcIJ(), hz.hops, hz.odo, default);

		// Note: ActLike: NumberTokenHundertTausend - CopyCode: 002 1
		public virtual List<Token> concat(NumberTokenTausend t, Token tt) => concat((NumberTokenTausendDrei)t, tt);

		// Note: ActLike: NumberTokenHundertTausend - CopyCode: 002 2
		public virtual List<Token> concat(NumberTokenTausendDrei f, Token t) {
			List<Token> ret = new List<Token>();

			if (f is NumberTokenTausendDrei && t != null && !(t is NumberToken)) {
				OwnDataObject newOdo = this.concatHelperMinusDecimalsCurrency(f.odo);
				NumberTokenHundertTausendFünf newToken = NumberTokenHundertTausendFünf.constructStatic(this.s + f.s, this.i * f.i, this.hops + f.hops, newOdo, f.j.Value);
				if (newOdo is OwnDataObject && newToken is NumberTokenHundertTausendFünf) {
					ret.Add(newToken);
					ret.Add(t);
				}
			}

			return ret;
		}
    }

	public class NumberTokenZwei : NumberToken {

		private NumberTokenZwei(string s, int i, int hops = 1, OwnDataObject odo = default, int? j = default) : base(s, i, hops, odo, j) { ; }

		public new static bool isValid(int i) => (0 <= i && i <= 99);

		public new static NumberTokenZwei constructStatic(string s, int i, int hops = 1, OwnDataObject odo = default, int? j = default) {
			if (isValid(i) && isValidJ(j))
				return new NumberTokenZwei(s, i, hops, odo, j);
			else
				return null;
		}

		public override NumberToken construct(string s, int i, int hops = 1, OwnDataObject odo = default, int? j = default) {
			return constructStatic(s, i, hops, odo, j);
		}

		// public static implicit operator NumberTokenHundertZwei(NumberTokenZwei z) => NumberTokenHundertZwei.constructStatic(z.s, 0, z.hops, z.odo, z.i);
		// public static implicit operator NumberTokenDrei(NumberTokenZwei z) => NumberTokenDrei.constructStatic(z.s, z.i, z.hops, z.odo, default);
		public static implicit operator NumberTokenDrei(NumberTokenZwei z) => NumberTokenDrei.constructStatic(z.s, z.i, z.hops, z.odo, default);
		public static implicit operator NumberTokenVier(NumberTokenZwei z) => NumberTokenVier.constructStatic(z.s, z.i, z.hops, z.odo, default);
		public static implicit operator NumberTokenFünf(NumberTokenZwei z) => NumberTokenFünf.constructStatic(z.s, z.i, z.hops, z.odo, default);
		public static implicit operator NumberTokenSechs(NumberTokenZwei z) => NumberTokenSechs.constructStatic(z.s, z.i, z.hops, z.odo, default);
		public static implicit operator NumberTokenSieben(NumberTokenZwei z) => NumberTokenSieben.constructStatic(z.s, z.i, z.hops, z.odo, default);
		public static implicit operator NumberTokenAcht(NumberTokenZwei z) => NumberTokenAcht.constructStatic(z.s, z.i, z.hops, z.odo, default);
		public static implicit operator NumberTokenNeun(NumberTokenZwei z) => NumberTokenNeun.constructStatic(z.s, z.i, z.hops, z.odo, default);
		public static implicit operator NumberTokenZehn(NumberTokenZwei z) => NumberTokenZehn.constructStatic(z.s, z.i, z.hops, z.odo, default);
		public virtual List<Token> concat(NumberTokenHundert h, Token t) {
			return concat( (NumberTokenHundertZwei)h , t);
		}

		public virtual List<Token> concat(NumberTokenHundertZwei hz, Token t) {
			List<Token> ret = new List<Token>();

			if (hz is NumberTokenHundertZwei && NumberTokenHundertZwei.isValidJ(hz.j) && t != null && ! (t is NumberToken)) {
				OwnDataObject newOdo = this.concatHelperMinusDecimalsCurrency(hz.odo);
				NumberTokenVier newToken = NumberTokenVier.constructStatic(this.s + hz.s, this.i * (hz.i * 100) + hz.j.Value, this.hops + hz.hops, newOdo, default);
				if (newOdo is OwnDataObject && newToken is NumberTokenVier) {
					ret.Add(newToken);
					ret.Add(t);
				}
			}

			return ret;
		}

		// public virtual List<Token> concat(NumberTokenMillionSechs ms, Token t) {
		// 	List<Token> ret = new List<Token>();
		// 	if (ms is NumberTokenMillionSechs && NumberTokenMillionSechs.isValidJ(ms.j) && t != null && ! (t is NumberToken)) {
		// 		OwnDataObject newOdo = this.concatHelperMinusDecimalsCurrency(ms.odo);
		// 		NumberTokenAcht newToken = NumberTokenAcht.constructStatic(this.s + ms.s, this.i * (ms.i * 1_000_000) + ms.j.Value, this.hops + ms.hops, newOdo, default);
		// 		if (newOdo is OwnDataObject && newToken is NumberTokenAcht) {
		// 			ret.Add(newToken);
		// 			ret.Add(t);
		// 		}
		// 	}

		// 	return ret;
		// }
	}

	public class NumberTokenDrei : NumberToken {

		private NumberTokenDrei(string s, int i, int hops = 1, OwnDataObject odo = default, int? j = default) : base(s, i, hops, odo, j) { ; }

		public new static bool isValid(int i) => (0 <= i && i <= 999);

		public new static NumberTokenDrei constructStatic(string s, int i, int hops = 1, OwnDataObject odo = default, int? j = default) {
			if (isValid(i) && isValidJ(j))
				return new NumberTokenDrei(s, i, hops, odo, j);
			else
				return null;
		}

		public override NumberToken construct(string s, int i, int hops = 1, OwnDataObject odo = default, int? j = default) {
			return constructStatic(s, i, hops, odo, j);
		}

		public static implicit operator NumberTokenVier(NumberTokenDrei d) => NumberTokenVier.constructStatic(d.s, d.i, d.hops, d.odo, default);
		public static implicit operator NumberTokenFünf(NumberTokenDrei d) => NumberTokenFünf.constructStatic(d.s, d.i, d.hops, d.odo, default);
		public static implicit operator NumberTokenSechs(NumberTokenDrei d) => NumberTokenSechs.constructStatic(d.s, d.i, d.hops, d.odo, default);
		public static implicit operator NumberTokenSieben(NumberTokenDrei d) => NumberTokenSieben.constructStatic(d.s, d.i, d.hops, d.odo, default);
		public static implicit operator NumberTokenAcht(NumberTokenDrei d) => NumberTokenAcht.constructStatic(d.s, d.i, d.hops, d.odo, default);
		public static implicit operator NumberTokenNeun(NumberTokenDrei d) => NumberTokenNeun.constructStatic(d.s, d.i, d.hops, d.odo, default);
		public static implicit operator NumberTokenZehn(NumberTokenDrei d) => NumberTokenZehn.constructStatic(d.s, d.i, d.hops, d.odo, default);

		// Note: ActLike: NumberTokenSechs
		public virtual List<Token> concat(NumberTokenTausendDrei td, Token t) {
			List<Token> ret = new List<Token>();

			if (td is NumberTokenTausendDrei && t != null && ! (t is NumberToken)) {
				
				OwnDataObject newOdo = this.concatHelperMinusDecimalsCurrency(td.odo);
				NumberTokenSechs newToken = NumberTokenSechs.constructStatic(this.s + td.s, this.i * 1_000 * td.i + td.j.Value, this.hops + td.hops, newOdo, default);
				if (newOdo is OwnDataObject && newToken is NumberTokenSechs) {
					ret.Add(newToken);
					ret.Add(t);
				}
			}

			return ret;
		}

		// Note: ActLike: NumberTokenFünf
		public virtual List<Token> concat(NumberTokenHundertZwei hz, Token t) {
			List<Token> ret = new List<Token>();

			if (hz is NumberTokenHundertZwei && t != null && ! (t is NumberToken)) {
				
				OwnDataObject newOdo = this.concatHelperMinusDecimalsCurrency(hz.odo);
				NumberTokenFünf newToken = NumberTokenFünf.constructStatic(this.s + hz.s, this.i * 100 * hz.i + hz.j.Value, this.hops + hz.hops, newOdo, default);
				if (newOdo is OwnDataObject && newToken is NumberTokenFünf) {
					ret.Add(newToken);
					ret.Add(t);
				}
			}

			return ret;
		}
		
	}

	public class NumberTokenTausend : NumberToken {

		private NumberTokenTausend(string s, int i, int hops = 1, OwnDataObject odo = default, int? j = default) : base(s, i, hops, odo, j) { ; }

		public new static NumberTokenTausend constructStatic(string s, int i, int hops = 1, OwnDataObject odo = default, int? j = default) {
			if (isValid(i) && isValidJ(j))
				return new NumberTokenTausend(s, i, hops, odo, j);
			else
				return null;
		}

		public override NumberToken construct(string s, int i, int hops = 1, OwnDataObject odo = default, int? j = default) {
			return constructStatic(s, i, hops, odo, j);
		}

		public override int calcIJ() => i * 1_000;

		public static implicit operator NumberTokenVier(NumberTokenTausend t) => NumberTokenVier.constructStatic(t.s, t.calcIJ(), t.hops, t.odo, default);

		public virtual List<Token> concat(NumberTokenEins e, Token t) {
			return concat( (NumberTokenZwei)e , t);
		}

		public virtual List<Token> concat(NumberTokenZehner z, Token t) {
			return concat( (NumberTokenZwei)z , t);
		}

		public virtual List<Token> concat(NumberTokenZwei z, Token t) {
			return concat( (NumberTokenDrei)z , t);
		}

		public virtual List<Token> concat(NumberTokenDrei d, Token t) {
			List<Token> ret = new List<Token>();

			if (d is NumberTokenDrei && t != null && ! (t is NumberToken)) {
				OwnDataObject newOdo = this.concatHelperMinusDecimalsCurrency(d.odo);
				NumberTokenTausendDrei newToken = NumberTokenTausendDrei.constructStatic(this.s + d.s, this.i, this.hops + d.hops, newOdo, d.i);
				if (newOdo is OwnDataObject && newToken is NumberTokenTausendDrei) {
					ret.Add(newToken);
					ret.Add(t);
				}
			}

			return ret;
		}
	}

	public class NumberTokenTausendDrei : NumberToken {

		private NumberTokenTausendDrei(string s, int i, int hops = 1, OwnDataObject odo = default, int? j = default) : base(s, i, hops, odo, j) { ; }

		public new static bool isValidJ(int? j) => (j.HasValue && 0 <= j.Value && j.Value <= 999);

		public new static NumberTokenTausendDrei constructStatic(string s, int i, int hops = 1, OwnDataObject odo = default, int? j = default) {
			if (isValid(i) && isValidJ(j))
				return new NumberTokenTausendDrei(s, i, hops, odo, j);
			else
				return null;
		}

		public override int calcIJ() => i * 1_000 + j.Value;

		//public static implicit operator NumberTokenTausendDrei(NumberTokenEins e) => NumberTokenTausendDrei.constructStatic(e.s, 0, e.hops, e.odo, e.i);
		//public static implicit operator NumberTokenTausendDrei(NumberTokenZwei z) => NumberTokenTausendDrei.constructStatic(z.s, 0, z.hops, z.odo, z.i);
		public static implicit operator NumberTokenVier(NumberTokenTausendDrei td) => NumberTokenVier.constructStatic(td.s, td.calcIJ(), td.hops, td.odo, default);

        public override NumberToken construct(string s, int i, int hops = 1, OwnDataObject odo = default, int? j = default) {
            return constructStatic(s, i, hops, odo, j);
        }

		public static implicit operator NumberTokenTausendDrei(NumberTokenDrei d) => NumberTokenTausendDrei.constructStatic(d.s, 0, d.hops, d.odo, d.i);

		public static implicit operator NumberTokenTausendDrei(NumberTokenTausend h) => NumberTokenTausendDrei.constructStatic(h.s, h.i, h.hops, h.odo, 0);
    }

	public class NumberTokenVier : NumberToken {
		private NumberTokenVier(string s, int i, int hops = 1, OwnDataObject odo = default, int? j = default) : base(s, i, hops, odo, j) { ; }

		public new static bool isValid(int i) => (0 <= i && i <= 9_999);

		public new static NumberTokenVier constructStatic(string s, int i, int hops = 1, OwnDataObject odo = default, int? j = default) {
			if (isValid(i) && isValidJ(j))
				return new NumberTokenVier(s, i, hops, odo, j);
			else
				return null;
		}

		public override NumberToken construct(string s, int i, int hops = 1, OwnDataObject odo = default, int? j = default) {
			return constructStatic(s, i, hops, odo, j);
		}

		public static implicit operator NumberTokenFünf(NumberTokenVier v) => NumberTokenFünf.constructStatic(v.s, v.i, v.hops, v.odo, default);
		public static implicit operator NumberTokenSechs(NumberTokenVier v) => NumberTokenSechs.constructStatic(v.s, v.i, v.hops, v.odo, default);
	}

	public class NumberTokenFünf : NumberToken {
		private NumberTokenFünf(string s, int i, int hops = 1, OwnDataObject odo = default, int? j = default) : base(s, i, hops, odo, j) { ; }

		public new static bool isValid(int i) => (0 <= i && i <= 99_999);

		public new static NumberTokenFünf constructStatic(string s, int i, int hops = 1, OwnDataObject odo = default, int? j = default) {
			if (isValid(i) && isValidJ(j))
				return new NumberTokenFünf(s, i, hops, odo, j);
			else
				return null;
		}

		public override NumberToken construct(string s, int i, int hops = 1, OwnDataObject odo = default, int? j = default) {
			return constructStatic(s, i, hops, odo, j);
		}

		public static implicit operator NumberTokenSechs(NumberTokenFünf f) => NumberTokenSechs.constructStatic(f.s, f.i, f.hops, f.odo, default);
	}

	public class NumberTokenHundertTausend : NumberToken {
		private NumberTokenHundertTausend(string s, int i, int hops = 1, OwnDataObject odo = default, int? j = default) : base(s, i, hops, odo, j) { ; }

		public new static NumberTokenHundertTausend constructStatic(string s, int i, int hops = 1, OwnDataObject odo = default, int? j = default) {
			if (isValid(i) && isValidJ(j))
				return new NumberTokenHundertTausend(s, i, hops, odo, j);
			else
				return null;
		}

		public override NumberToken construct(string s, int i, int hops = 1, OwnDataObject odo = default, int? j = default) {
			return constructStatic(s, i, hops, odo, j);
		}

		public override int calcIJ() => i * 100_000;

		public static implicit operator NumberTokenHundertTausendFünf(NumberTokenHundertTausend h) => NumberTokenHundertTausendFünf.constructStatic(h.s, h.i, h.hops, h.odo, 0);
		public static implicit operator NumberTokenSechs(NumberTokenHundertTausend h) => NumberTokenSechs.constructStatic(h.s, h.calcIJ(), h.hops, h.odo, default);

		public virtual List<Token> concat(NumberTokenVier v, Token t) {
			return concat( (NumberTokenFünf)v , t);
		}

		//public virtual List<Token> concat(NumberTokenFünf z, Token t) {
		//	return concat( (NumberTokenFünf)z , t);
		//}

		public virtual List<Token> concat(NumberTokenFünf td, Token t) {
			List<Token> ret = new List<Token>();

			if (td is NumberTokenFünf && t != null && ! (t is NumberToken)) {
				OwnDataObject newOdo = this.concatHelperMinusDecimalsCurrency(td.odo);
				NumberTokenHundertTausendFünf newToken = NumberTokenHundertTausendFünf.constructStatic(this.s + td.s, this.i * 100_000, this.hops + td.hops, newOdo, td.i);
				if (newOdo is OwnDataObject && newToken is NumberTokenHundertTausendFünf) {
					ret.Add(newToken);
					ret.Add(t);
				}
			}

			return ret;
		}
	}

	public class NumberTokenHundertTausendFünf : NumberToken {

		private NumberTokenHundertTausendFünf(string s, int i, int hops = 1, OwnDataObject odo = default, int? j = default) : base(s, i, hops, odo, j) { ; }

		public new static bool isValidJ(int? j) => (j.HasValue && 0 <= j.Value && j.Value <= 99_999);

		public new static NumberTokenHundertTausendFünf constructStatic(string s, int i, int hops = 1, OwnDataObject odo = default, int? j = default) {
			if (isValid(i) && isValidJ(j))
				return new NumberTokenHundertTausendFünf(s, i, hops, odo, j);
			else
				return null;
		}

        public override NumberToken construct(string s, int i, int hops = 1, OwnDataObject odo = default, int? j = default) {
            return constructStatic(s, i, hops, odo, j);
        }

		public override int calcIJ() => i * 100_000 + j.Value;

		public static implicit operator NumberTokenSechs(NumberTokenHundertTausendFünf hz) => NumberTokenSechs.constructStatic(hz.s, hz.calcIJ(), hz.hops, hz.odo, default);
    }

	public class NumberTokenSechs : NumberToken {
		private NumberTokenSechs(string s, int i, int hops = 1, OwnDataObject odo = default, int? j = default) : base(s, i, hops, odo, j) { ; }

		public new static bool isValid(int i) => (0 <= i && i <= 999_999);

		public new static NumberTokenSechs constructStatic(string s, int i, int hops = 1, OwnDataObject odo = default, int? j = default) {
			if (isValid(i) && isValidJ(j))
				return new NumberTokenSechs(s, i, hops, odo, j);
			else
				return null;
		}

		public override NumberToken construct(string s, int i, int hops = 1, OwnDataObject odo = default, int? j = default) {
			return constructStatic(s, i, hops, odo, j);
		}

		public static implicit operator NumberTokenSieben(NumberTokenSechs s) => NumberTokenSieben.constructStatic(s.s, s.i, s.hops, s.odo, default);
	}

	public class NumberTokenMillion : NumberToken {

		private NumberTokenMillion(string s, int i, int hops = 1, OwnDataObject odo = default, int? j = default) : base(s, i, hops, odo, j) { ; }

		public new static NumberTokenMillion constructStatic(string s, int i, int hops = 1, OwnDataObject odo = default, int? j = default) {
			if (isValid(i) && isValidJ(j))
				return new NumberTokenMillion(s, i, hops, odo, j);
			else
				return null;
		}

		public override NumberToken construct(string s, int i, int hops = 1, OwnDataObject odo = default, int? j = default) {
			return constructStatic(s, i, hops, odo, j);
		}

		public override int calcIJ() => i * 1_000_000;

		public static implicit operator NumberTokenMillionSechs(NumberTokenMillion m) => NumberTokenMillionSechs.constructStatic(m.s, m.i, m.hops, m.odo, 0);
		public static implicit operator NumberTokenSieben(NumberTokenMillion m) => NumberTokenSieben.constructStatic(m.s, m.calcIJ(), m.hops, m.odo, default);

		public virtual List<Token> concat(NumberTokenSechs s, Token t) {
			List<Token> ret = new List<Token>();

			if (s is NumberTokenSechs && t != null && ! (t is NumberToken)) {
				OwnDataObject newOdo = this.concatHelperMinusDecimalsCurrency(s.odo);
				NumberTokenMillionSechs newToken = NumberTokenMillionSechs.constructStatic(this.s + s.s, this.i, this.hops + s.hops, newOdo, s.i);
				if (newOdo is OwnDataObject && newToken is NumberTokenMillionSechs) {
					ret.Add(newToken);
					ret.Add(t);
				}
			}

			return ret;
		}

		// Note: ActLike: LeftSide - CopyCode: 003 1
		public virtual List<Token> concat(NumberTokenEins e, NumberTokenHundert t) => this.concat( (NumberTokenDrei)e , t);
		// Note: ActLike: LeftSide - CopyCode: 003 2
		public virtual List<Token> concat(NumberTokenZehner z, NumberTokenHundert t) => this.concat( (NumberTokenDrei)z, t);
		// Note: ActLike: LeftSide - CopyCode: 003 3
		public virtual List<Token> concat(NumberTokenZwei z, NumberTokenHundert t) => this.concat( (NumberTokenDrei)z, t);
		// Note: ActLike: LeftSide - CopyCode: 003 4
		public virtual List<Token> concat(NumberTokenHundert h, NumberTokenHundert t) => this.concat( (NumberTokenDrei)h, t);
		// Note: ActLike: LeftSide - CopyCode: 003 5
		public virtual List<Token> concat(NumberTokenHundertZwei hz, NumberTokenHundert t) => this.concat( (NumberTokenDrei)hz, t);

		// Note: ActLike: LeftSide - ActLike: NumberTokenFünf - CopyCode: 003 6
		public virtual List<Token> concat(NumberTokenDrei d, NumberTokenHundert t) {
			List<Token> ret = new List<Token>();

			if (d is NumberTokenDrei) {
				ret.Add(this);
				ret.Add(d);
				ret.Add(t);
			}

			return ret;
		}

		// Note: ActLike: LeftSide - CopyCode: 003 1
		public virtual List<Token> concat(NumberTokenEins e, NumberTokenHundertZwei t) => this.concat( (NumberTokenDrei)e , t);
		// Note: ActLike: LeftSide - CopyCode: 003 2
		public virtual List<Token> concat(NumberTokenZehner z, NumberTokenHundertZwei t) => this.concat( (NumberTokenDrei)z, t);
		// Note: ActLike: LeftSide - CopyCode: 003 3
		public virtual List<Token> concat(NumberTokenZwei z, NumberTokenHundertZwei t) => this.concat( (NumberTokenDrei)z, t);
		// Note: ActLike: LeftSide - CopyCode: 003 4
		public virtual List<Token> concat(NumberTokenHundert h, NumberTokenHundertZwei t) => this.concat( (NumberTokenDrei)h, t);
		// Note: ActLike: LeftSide - CopyCode: 003 5
		public virtual List<Token> concat(NumberTokenHundertZwei hz, NumberTokenHundertZwei t) => this.concat( (NumberTokenDrei)hz, t);

		// Note: ActLike: LeftSide - ActLike: NumberTokenFünf - CopyCode: 003 6
		public virtual List<Token> concat(NumberTokenDrei d, NumberTokenHundertZwei t) {
			List<Token> ret = new List<Token>();

			if (d is NumberTokenDrei) {
				ret.Add(this);
				ret.Add(d);
				ret.Add(t);
			}

			return ret;
		}

		// Note: ActLike: LeftSide - CopyCode: 001 1
		public virtual List<Token> concat(NumberTokenEins e, NumberTokenTausend t) => this.concat( (NumberTokenDrei)e , t);
		// Note: ActLike: LeftSide - CopyCode: 001 2
		public virtual List<Token> concat(NumberTokenZehner z, NumberTokenTausend t) => this.concat( (NumberTokenDrei)z, t);
		// Note: ActLike: LeftSide - CopyCode: 001 3
		public virtual List<Token> concat(NumberTokenZwei z, NumberTokenTausend t) => this.concat( (NumberTokenDrei)z, t);
		// Note: ActLike: LeftSide - CopyCode: 001 4
		public virtual List<Token> concat(NumberTokenHundert h, NumberTokenTausend t) => this.concat( (NumberTokenDrei)h, t);
		// Note: ActLike: LeftSide - CopyCode: 001 5
		public virtual List<Token> concat(NumberTokenHundertZwei hz, NumberTokenTausend t) => this.concat( (NumberTokenDrei)hz, t);

		// Note: ActLike: LeftSide - CopyCode: 001 6
		public virtual List<Token> concat(NumberTokenDrei d, NumberTokenTausend t) {
			List<Token> ret = new List<Token>();

			if (d is NumberTokenDrei) {
				ret.Add(this);
				ret.Add(d);
				ret.Add(t);
			}

			return ret;
		}

		// Note: ActLike: LeftSide - CopyCode: 001 1
		public virtual List<Token> concat(NumberTokenEins e, NumberTokenTausendDrei td) => this.concat( (NumberTokenDrei)e , td);
		// Note: ActLike: LeftSide - CopyCode: 001 2
		public virtual List<Token> concat(NumberTokenZehner z, NumberTokenTausendDrei td) => this.concat( (NumberTokenDrei)z, td);
		// Note: ActLike: LeftSide - CopyCode: 001 3
		public virtual List<Token> concat(NumberTokenZwei z, NumberTokenTausendDrei td) => this.concat( (NumberTokenDrei)z, td);
		// Note: ActLike: LeftSide - CopyCode: 001 4
		public virtual List<Token> concat(NumberTokenHundert h, NumberTokenTausendDrei td) => this.concat( (NumberTokenDrei)h, td);
		// Note: ActLike: LeftSide - CopyCode: 001 5
		public virtual List<Token> concat(NumberTokenHundertZwei hz, NumberTokenTausendDrei td) => this.concat( (NumberTokenDrei)hz, td);

		// Note: ActLike: LeftSide - CopyCode: 001 6
		public virtual List<Token> concat(NumberTokenDrei d, NumberTokenTausendDrei td) {
			List<Token> ret = new List<Token>();

			if (d is NumberTokenDrei) {
				ret.Add(this);
				ret.Add(d);
				ret.Add(td);
			}

			return ret;
		}
	}

	public class NumberTokenMillionSechs : NumberToken {

		private NumberTokenMillionSechs(string s, int i, int hops = 1, OwnDataObject odo = default, int? j = default) : base(s, i, hops, odo, j) { ; }

		public new static bool isValidJ(int? j) => (j.HasValue && 0 <= j.Value && j.Value <= 999_999);

		public new static NumberTokenMillionSechs constructStatic(string s, int i, int hops = 1, OwnDataObject odo = default, int? j = default) {
			if (isValid(i) && isValidJ(j))
				return new NumberTokenMillionSechs(s, i, hops, odo, j);
			else
				return null;
		}

		public override int calcIJ() => i * 1_000_000 + j.Value;

        public override NumberToken construct(string s, int i, int hops = 1, OwnDataObject odo = default, int? j = default) {
            return constructStatic(s, i, hops, odo, j);
        }

		public static implicit operator NumberTokenSieben(NumberTokenMillionSechs ms) => NumberTokenSieben.constructStatic(ms.s, ms.calcIJ(), ms.hops, ms.odo, default);
    }

	public class NumberTokenSieben : NumberToken {
		private NumberTokenSieben(string s, int i, int hops = 1, OwnDataObject odo = default, int? j = default) : base(s, i, hops, odo, j) { ; }

		public new static bool isValid(int i) => (0 <= i && i <= 9_999_999);

		public new static NumberTokenSieben constructStatic(string s, int i, int hops = 1, OwnDataObject odo = default, int? j = default) {
			if (isValid(i) && isValidJ(j))
				return new NumberTokenSieben(s, i, hops, odo, j);
			else
				return null;
		}

		public override NumberToken construct(string s, int i, int hops = 1, OwnDataObject odo = default, int? j = default) {
			return constructStatic(s, i, hops, odo, j);
		}
	}

	public class NumberTokenAcht : NumberToken {
		private NumberTokenAcht(string s, int i, int hops = 1, OwnDataObject odo = default, int? j = default) : base(s, i, hops, odo, j) { ; }

		public new static bool isValid(int i) => (0 <= i && i <= 99_999_999);

		public new static NumberTokenAcht constructStatic(string s, int i, int hops = 1, OwnDataObject odo = default, int? j = default) {
			if (isValid(i) && isValidJ(j))
				return new NumberTokenAcht(s, i, hops, odo, j);
			else
				return null;
		}

		public override NumberToken construct(string s, int i, int hops = 1, OwnDataObject odo = default, int? j = default) {
			return constructStatic(s, i, hops, odo, j);
		}

		public static implicit operator NumberTokenNeun(NumberTokenAcht a) => NumberTokenNeun.constructStatic(a.s, a.i, a.hops, a.odo, default);
	}

	public class NumberTokenNeun : NumberToken {
		private NumberTokenNeun(string s, int i, int hops = 1, OwnDataObject odo = default, int? j = default) : base(s, i, hops, odo, j) { ; }

		public new static bool isValid(int i) => (0 <= i && i <= 999_999_999);

		public new static NumberTokenNeun constructStatic(string s, int i, int hops = 1, OwnDataObject odo = default, int? j = default) {
			if (isValid(i) && isValidJ(j))
				return new NumberTokenNeun(s, i, hops, odo, j);
			else
				return null;
		}

		public override NumberToken construct(string s, int i, int hops = 1, OwnDataObject odo = default, int? j = default) {
			return constructStatic(s, i, hops, odo, j);
		}

		public static implicit operator NumberTokenZehn(NumberTokenNeun n) => NumberTokenZehn.constructStatic(n.s, n.i, n.hops, n.odo, default);
	}

	public class NumberTokenZehn : NumberToken {
		private NumberTokenZehn(string s, int i, int hops = 1, OwnDataObject odo = default, int? j = default) : base(s, i, hops, odo, j) { ; }

		public new static bool isValid(int i) => (0 <= i && i <= 9_999_999_999); // ToDo: int -> longint

		public new static NumberTokenZehn constructStatic(string s, int i, int hops = 1, OwnDataObject odo = default, int? j = default) {
			if (isValid(i) && isValidJ(j))
				return new NumberTokenZehn(s, i, hops, odo, j);
			else
				return null;
		}

		public override NumberToken construct(string s, int i, int hops = 1, OwnDataObject odo = default, int? j = default) {
			return constructStatic(s, i, hops, odo, j);
		}

		//public static implicit operator NumberTokenElf(NumberTokenZehn z) => NumberTokenElf.constructStatic(z.s, z.i, z.hops, z.odo, default);
	}

	public class NumberTokenDecimals : NumberToken {
		private NumberTokenDecimals(string s, int i, int hops = 1, OwnDataObject odo = default, int? j = default) : base(s, i, hops, odo, j) { ; }

		public new static bool isValid(int i) => (0 <= i);

		public new static NumberTokenDecimals constructStatic(string s, int i, int hops = 1, OwnDataObject odo = default, int? j = default) {
			if (isValid(i) && isValidJ(j))
				return new NumberTokenDecimals(s, i, hops, odo, j);
			else
				return null;
		}

		public override NumberToken construct(string s, int i, int hops = 1, OwnDataObject odo = default, int? j = default) { 
			return constructStatic(s, i, hops, odo, j);
		}

		public override string ToString(int debug = 0, string special = default) {
			if (debug == 0)
				return "," + base.ToString(0);
			else if (debug == 1)
				return  "[" + ToString(0) + "]";
			else //if (debug == 2)
				return base.ToString(2);
		}
	}

	public class ErrorToken : Token {
		public string message { get; } // ToDo
		private ErrorToken(string s, string message, int hops = default) : base(s, hops) {
			this.message = message;
		}

		public static ErrorToken constructStatic(string s, string message, int hops = default) {
			return new ErrorToken(s, message);
		}

		public override string ToString(int debug = 0, string special = default) {
			if (debug == 0)
				return s;
			else if (debug == 1)
				return "[" + ToString(0) + "]";
			else //if (debug == 2)
				return ToString(1) + " - [" + this.GetType().ToString().Substring(17, this.GetType().ToString().Length - 17) + " | s: \"" + s + "\"]";
		}
	}

	public class Words_to_Numbers {

		//     0 1 2 3 4
		// s = a b c d e		Condition
		//             s		s < length
		//             s l=1	s + l <= length
		//
		public string mySubString(string s, int start, int length) {
			if (start >= s.Length)
				start = start; // Note
			if (start + length > s.Length)
				length = s.Length - start;

			return s.Substring(start, length);
		}

		// Adjustment for the input of token-generator.
		public string adjustment(string s) {
			return s.ToLower();
		}

		// ToDo
		public List<Token> stringToTokens(string input, Dictionary<string, Token> dictCharacters, Dictionary<string, Token> dictStrings, int stepWidth = 20) { // ToDo: longest possible Word -> 15 besser 20 ?
			List<Token> ret = new List<Token>();

			string buffer;
			Token t;
			int iSave = 0;
			for (int i=0; i < input.Length; ) {

				// Single characters
				buffer = input.Substring(i, 1);
				
				//ICollection<string> characters = dictCharacters.Keys.Where(entry => buffer.Equals(entry)).ToList();
				//if (characters.Count() >= 1) {
				if (dictCharacters.ContainsKey(buffer)) {
					t = dictCharacters[buffer];
					if (iSave < i) { // if Token can be builded, the characters before build a TextToken
						buffer = input.Substring(iSave, i - iSave);
						ret.Add(new TextToken(buffer));
					}
					ret.Add(t);
					i += 1;
					iSave = i;
					continue;
				}

				// Long string, up to whole stepWidth
				buffer = input.Substring(i, (i + stepWidth <= input.Length) ? stepWidth : input.Length - i);
				ICollection<string> strings = dictStrings.Keys.Where(entry => adjustment(buffer).StartsWith(entry)).ToList();
				if (strings.Count() >= 1) {
					if (iSave < i) { // if Token can be builded, the characters before build a TextToken
						buffer = input.Substring(iSave, i - iSave);
						ret.Add(new TextToken(buffer));
					}

					int longest = 0;
					string longestString = default;
					foreach (string s in strings) {
						if (s.Length > longest) {
							longest = s.Length;
							longestString = s;
						}
					}
//					ret.Add(strings.First());
					t = dictStrings[longestString];
					ret.Add(t);
					i += longest;
					iSave = i;
					continue;
				}

				// Reached stepWidth
				if ((i - iSave) >= stepWidth) {
					buffer = input.Substring(iSave, (i + stepWidth <= input.Length) ? stepWidth : input.Length - i);
					ret.Add(new TextToken(buffer));
					//i += stepWidth;
					iSave = i;
					continue;
				}

				// End of input
				//else if (i + stepWidth >= input.Length - 1) {
				//	buffer = input.Substring(iSave, (i + stepWidth <= input.Length) ? stepWidth : input.Length - i);
				else if (i == input.Length - 1) {
					buffer = input.Substring(iSave, input.Length - iSave);
					ret.Add(new TextToken(buffer));
					i += stepWidth;
					iSave = i;
					continue;
				}
				
				i++;
			}

			return ret;
		}

//	.Keys.Where(k => word.StartsWith(k)).OrderByDescending(p => p.Length).FirstOrDefault()?.debugOut();

		public List<Token> concatenatorStep(Token t, Token tt, Token ttt, int debug = 0) {
			List<Token> ret = new List<Token>();

			if (tt != null && ttt != null) {

				// Two TextTokens (third will be ignored) 
				if (tt is TextToken && ttt is TextToken) { // ToDo: remove all concat-functions with one parameter
					TextToken newText = (tt as TextToken).concat(ttt as TextToken);
					if (newText is Token) {
						ret.Add(t);
						ret.Add(newText);
						return ret;
					}
				}

				// Three Tokens - dynamic for calling the right method (parameters have influence too)
				dynamic dT = t;
				dynamic dTT = tt;
				dynamic dTTT = ttt;
				if (
					dT is TextToken ||

					//dT is WhiteSpaceToken - can't do anything on it's own
					dT is UndToken  ||

					dT is MinusToken ||
					dT is SeperatorToken ||
					dT is NumberTokenDecimals ||
					dT is CurrencyToken ||

					// dT ist NumberToken - is abstract

					dT is NumberTokenSimple ||

					dT is NumberTokenEins ||				// z

					dT is NumberTokenZehner ||				// y * 10
					dT is NumberTokenZwei ||				// yz

					dT is NumberTokenHundert ||				// x * 100
					dT is NumberTokenHundertZwei ||			// x * 100 + yz
					dT is NumberTokenDrei ||				// xyz

					dT is NumberTokenTausend ||				// w * 1000
					dT is NumberTokenTausendDrei ||			// w * 1000 + xyz
					dT is NumberTokenVier ||				// w.xyz

					dT is NumberTokenFünf ||				// vw.xyz
					dT is NumberTokenHundertTausend ||		// u * 100 000
					dT is NumberTokenHundertTausendFünf ||	// u * 100 000 + vw.xyz
					dT is NumberTokenSechs ||				// uvw.xyz

					dT is NumberTokenSieben ||				// t.uvw.xyz

					dT is NumberTokenMillion ||				// t * 100 0 000
					dT is NumberTokenMillionSechs ||		// t * 100 0 000 + uv w.xyz
					dT is NumberTokenSieben ||				// t.uv w.xyz

					dT is NumberTokenAcht ||				// st.uvw.xyz
					dT is NumberTokenNeun ||				// rst.uvw.xyz

					//dT is NumberTokenMilliarde ||			// q * 100 0 00 0 000
					//dT is NumberTokenMilliardeNeun ||		// q * 100 0 00 0 000 + rs t.uv w.xyz
					dT is NumberTokenZehn ||				// q.rs t.uv w.xyz
					
					dT is ErrorToken
				) {
					List<Token> concat = dT.concat(dTT, dTTT);
					if (concat.Count >= 1) {
						return concat;
					}
				}
			}
			
			if (debug >= 1)
				Console.WriteLine("concatenatorStep(): no concatenation: " + (t != null ? t.ToString(1) : "no t") + " " + (tt != null ? tt.ToString(1) : "no tt") + " " + (ttt != null ? ttt.ToString(1) : "no ttt"));
			ret.Add(t);
			ret.Add(tt);
			ret.Add(ttt);

			return ret;
		}

		public List<Token> concatenator(List<Token> work, int debug = 0) {
			List<Token> done = new List<Token>();

			while (work.Count >= 1) {
				// when 2 or more than do, otherwise end progress
				if (work.Count >= 2) {

					// declare variables
					Token t = default, tt, ttt;
					ttt = work.Last();
					work.RemoveAt(work.Count - 1);
					tt = work.Last();
					work.RemoveAt(work.Count - 1);
					if (work.Count >= 1) {
						t = work.Last();
						work.RemoveAt(work.Count - 1);
					}

					// concatenate them
					List<Token> concat = concatenatorStep(t, tt, ttt, debug);
					if (concat[0] is Token) // set first back to work
						work.Add(concat[0]);
					if (concat.Count >= 2) // set second back to work
						work.Add(concat[1]);
					if (concat.Count >= 3) // by success there is no third, otherwise to done
						done.Insert(0, concat[2]);
				} else {
					if (work.Count == 1) {
						done.Insert(0, work.First());
						work.RemoveAt(0);
					}
				}
			}

			return done;
		}

		public List<Token> run(string text, int numberOfRuns = 1, int debug = 0) {
			List<Token> tokens = stringToTokens(text, Dictionaries.Characters, Dictionaries.Strings);

			if (debug == 1) {
				Console.WriteLine("\n - - - - - - - - - - - - - - Test - - - - - - - - - - - - - - \n");
				//Console.WriteLine(text + "\n");
				
				// Console.WriteLine("\nText: " + text);

				// Console.WriteLine("\nTokens: ");
				// foreach (Token t in tokens)
				// 	Console.WriteLine("    " + t.ToString(2));
			}

			for (int i = 1; i <= numberOfRuns && i <= 10; i++)
				tokens = concatenator(tokens, debug);

			if (debug == 1) {
				Console.WriteLine("\n          Text: " + text);

				Console.Write("\n      New text: ");
				foreach (Token t in tokens)
					Console.Write(t.ToString(1));

				Console.Write("\n\nText in Tokens: ");
				foreach (Token t in tokens)
					Console.Write(t.s);

				Console.Write("\n\nTokens: ");
				foreach (Token t in tokens)
					Console.Write("\n    " + t.ToString(2));

				Console.WriteLine();
			}

			return tokens;
		}

		public List<Token> filter(List<Token> tokens, int level = 2, int debug = 0) {
			List<Token> ret = new List<Token>();

			foreach (Token t in tokens)
				if (t.hops < level) {
					ret.Add(TextToken.constructStatic(t.s));
					if (debug > 0)
						Console.WriteLine("filter: out " + t.ToString(debug));
				}
				else
					ret.Add(t);

			return ret;
		}
	}
}
