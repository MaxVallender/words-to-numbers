using System;
using System.Collections.Generic;

namespace Words_to_Numbers {
	class Program {
		static void Main(string[] args) {
			Words_to_Numbers wtn = new Words_to_Numbers();

			string text = "Hallo, dies ist ein Test-String. Drei hundert vier und dreißig Euro. Weißt du Bescheid?";
			     //text = "0123456789a123456789b123456789c123456789d123456789e123456789_123456789";
			wtn.run(text, 3, 1);

			wtn.run("Hallo, sie wollen Drei hundert zwölf Euro auf ihr Konto einzahlen. Dies sind Vier hundert zwei und dreißig Euro.", 3, 1);
			
			wtn.run("HACK Euro Zwei und dreißig HACK hack", 3, 1);

// ToDo: need multiple runs
			wtn.run("drei hundert zwölf -> 3 1 2 hack - Serh, sehr langer Text, auch wenn ein Wort falsch geschrieben ist. Er ist immer noch lange! AlsoEinWortUndDasGanzLangeZumTestenDerFunktionenDeshalbKommtNochmehrText.", 3, 1);
			
			wtn.run("Hier, einen Euro und zweiunddreißig Cent. Dutzend davon, sind nicht halbe-halbe oder fünfzig-fünzig zB.", 3, 1);
// fünfzig, sonst ok
			wtn.run("Betrag von in Euro fünfhundert zweiunddreißig.", 2, 1);

			wtn.run("Einmal die Hälfte davon, oder Quatsch ein viertel reicht auch.", 2, 1);

			wtn.run("Betrag von in Euro 2.345,678.", 2, 1);

			wtn.run("hack -01234,56789 hack", 2, 1);
// wollen beide decimals ToString
			wtn.run("einmal", 1, 1);

			wtn.run("ein", 1, 1);
// hops ausgeben und anwenden
			wtn.run("Hallo, dies ist ein Text, der Zahlen enthält zB die 1 oder auch die vierhundertfünfund dreißig Euro, vier Cent oder auch nicht.", 2, 1);

			wtn.run("hack - Fünfzehnhundert dreiundvierzig hack viertausend dreiund zwanzig ⁒ hack - hundert acht,vier cm hack cent - 12,3, 4 5,6 7 Euro - hack ", 4, 1);
// 4 wegen whitespace, 2 reichen sonst


			wtn.run("hack Sechs und zwanzig Tausend drei hack zwanzig Tausend hack vier und Million vierzig tausend eins hack", 8, 1);
// eins hinten, geschummelt, geht jetzt (unten), braucht nach wie vor Millionen
			wtn.run("hack millionen vierzig tausend hack", 8, 1);

			wtn.run("hack tausend vier hundert Euro zwei und dreißig hack", 8, 1);

			wtn.run("hack AchtMillionen dreihunderttausend vierUNDfünfzig hack ", 4, 1);

			wtn.run("hack vierhundert zehn millionen hack", 5, 1);


			//// ToDo:
			//			wtn.run("Hallo der fünfhundert zwei unddreißig, 5 fünf Euro hier.", 4, 1);


			/** Code example 1 */
			Words_to_Numbers wtn1 = new Words_to_Numbers();

			string textWithNumbers = "Zahlen wie minus null vier millionen achtzehnhundert zweiUNDdreißig Euro oder auch - 0 4 00 18 32,123 4 € gehen, geradeso.";

			int debugLevel = 0; // debugLevel is optional: 0, 1, 2 - standard: 0

			List<Token> liste = wtn.run(textWithNumbers, 4, 0);

			foreach (Token t in liste)
				Console.WriteLine(t.ToString(debugLevel));
			
			foreach (Token t in wtn.concatenator(wtn.filter(liste, 2, debugLevel), debugLevel))
				Console.WriteLine(t.ToString(debugLevel));

			Console.WriteLine("\n - - - - - - - - - - - - - - Dictionary - - - - - - - - - - - - - - \n");

			//foreach (var p in Dictionaries.Characters)
			//	Console.WriteLine("[" + p.Key + "] -> [" + p.Value.GetType().ToString().Substring(17, p.Value.GetType().ToString().Length - 17) + "]");
			//foreach (var p in Dictionaries.Strings)
			//	Console.WriteLine("[" + p.Key + "] -> [" + p.Value.GetType().ToString().Substring(17, p.Value.GetType().ToString().Length - 17) + "]");
		}
	}
}
